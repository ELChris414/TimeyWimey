from fuzzywuzzy import fuzz
import datetime
import dateutil.parser
import heapq

from globalz import *
import secret # Module that contains token
import setup

DB.execute("""CREATE TABLE IF NOT EXISTS users (
    user_id INTEGER NOT NULL PRIMARY KEY,
    hour24 INTEGER NOT NULL,
    zone TEXT NOT NULL
)""")
DB.commit()

async def im_valid(me):
    if not me.exists:
        await CLIENT.send_message(msg.channel, "You have no timezone configured.")
        return False
    if me.invalid:
        await CLIENT.send_message(msg.channel, "Your data is corrupt, somehow. You'll unfortunately need to re-setup.")
        return False
    return True
async def get_user(msg, args, default=None):
    member = default
    if args:
        search = " ".join(args)
        heap = []
        lookup = {}
        for u in msg.server.members:
            score = fuzz.partial_ratio(search, f"<@{u.id}> {u.nick} {u.name}#{u.discriminator}")
            heapq.heappush(heap, (-score, u.id))
            lookup[u.id] = u
        try:
            best = heapq.heappop(heap)
            member = lookup[best[1]]
        except (IndexError, KeyError):
            await CLIENT.send_message(msg.channel, "No such user!")
            return (None, None)

    if member is None:
        await CLIENT.send_message(msg.channel, "No such user!")
        return (None, None)
    target = User(member.id)
    if not target.exists:
        await CLIENT.send_message(msg.channel, "User has no timezone configured.")
        return (None, None)
    if target.invalid:
        await CLIENT.send_message(msg.channel, "User data is corrupt, somehow. They'll unfortunately need to re-setup.")
        return (None, None)
    return (target, member)
def get_clock(date):
    ROUND = 60*30
    try:
        date = datetime.datetime.fromtimestamp(round(date.timestamp() / ROUND) * ROUND, tz = date.tzinfo)
    except (OverflowError, OSError):
        return ""
    return ":clock" + str(date.hour % 12) + str(date.minute).lstrip("0") + ":" # lstrip because we want empty if 0

@CLIENT.event
async def on_message(msg):
    if msg.channel.is_private or msg.author.bot:
        return

    parts = msg.content.split()
    if len(parts) < 2 or parts[0] != "tw":
        return
    cmd = parts[1]
    args = parts[2:]

    if cmd == "setup":
        await setup.setup(msg)
    elif cmd == "timefor":
        me = User(msg.author.id)
        target, member = await get_user(msg, args, default=msg.author)
        if target is None:
            return

        date = datetime.datetime.now(target.zone)
        now = me.strftime(date)
        offset = utils.time(target.zone.utcoffset(datetime.datetime.now()))
        clock = get_clock(date)
        await CLIENT.send_message(msg.channel, embed =
            discord.Embed(
                title = f"Time in {target.zone_str}",
                description = f"Current time is {now} {clock}"
            )
            .set_footer(text = f"UTC offset is {offset.hours:+03}:{offset.minutes:02}", icon_url = member.avatar_url)
        )
    elif cmd == "whenis":
        me = User(msg.author.id)
        separator = None
        try:
            separator = args.index("for")
            if len(args) < 3 or len(args) == separator+1:
                raise ValueError("not enough arguments")
        except ValueError:
            await CLIENT.send_message(msg.channel, "Usage: tw whenis <date> for <user>")
            return
        target, _ = await get_user(msg, args[separator+1:])
        if target is None or not await im_valid(me):
            return

        now = datetime.datetime.now(me.zone)
        date = None
        try:
            date = dateutil.parser.parse(" ".join(args[:separator]), default = now, fuzzy = True)
        except (ValueError, OverflowError):
            await CLIENT.send_message(msg.channel, "Could not parse date. Example: 'Tomorrow at 5 PM'")
            return
        date = target.strftime(date.astimezone(target.zone))

        await CLIENT.send_message(msg.channel, f"Time in {target.zone_str} is {date}")
    elif cmd == "help":
        await CLIENT.send_message(msg.channel, embed =
            discord.Embed(title = "TimeyWimey help", description =
                "Welcome to the rewrite of TimeyWimey. This bot will probably "
                "crash a lot because it is written in python with no compile "
                " time checking.")
                .add_field(name = "tw setup", value = "Opens an interactive dialog to setup your timezone.")
                .add_field(name = "tw timefor", value = "Prints out the timezone and time for a user")
                .add_field(name = "tw whenis", value = "Prints out the time for a user at a specified time")
        )

CLIENT.run(secret.TOKEN)
DB.close()
