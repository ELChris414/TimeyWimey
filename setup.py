from globalz import *

import datetime
import dateutil.parser
import dateutil.tz

TIMEOUT = 60 * 5
TIMEOUT_STR = "5 minutes"

OFFSET_ROUND = 60 * 15

async def setup(msg):
    REACTIONS = "🕐🗺❓✅"
    sent = None

    sent = await CLIENT.send_message(msg.channel, embed = discord.Embed(
        title = "TimeyWimey Setup 1/2",
        description =
            "To set up your timezone, choose a few settings.\n"
            "\n"
            ":clock1: - Enable 24-hour display.\n"
            "\n"
            ":map: - Enter your timezone following the IANA time zone format\n"
            ":question: - Figure out your timezone by entering the current time\n"
            "\n"
            ":white_check_mark: - Proceed to next step\n"
    ))
    for emoji in REACTIONS:
        await CLIENT.add_reaction(sent, emoji)

    react = await CLIENT.wait_for_reaction(
        emoji = '✅',
        user = msg.author,
        message = sent,
        check = lambda react, user: react.emoji in REACTIONS,
        timeout = TIMEOUT
    )
    if react is None:
        await CLIENT.edit_message(sent, embed = discord.Embed(
            title = "Timeout",
            description = f"More than {TIMEOUT_STR} of inactivity"
        ))
        return

    sent = await CLIENT.get_message(sent.channel, sent.id)

    reacted = set()
    for reaction in filter(lambda react: react.emoji in REACTIONS, sent.reactions):
        users = await CLIENT.get_reaction_users(reaction)
        if msg.author in users:
            reacted.add(reaction.emoji)

    hour24 = '🕐'in reacted
    name = '🗺' in reacted
    guess = '❓' in reacted

    try:
        await CLIENT.clear_reactions(sent)
    except discord.Forbidden:
        pass

    if not name and not guess:
        await CLIENT.edit_message(sent, embed = discord.Embed(
            title = "Cancelled",
            description = "No mode was selected, so setup wizard was cancelled."
        ))
        return

    reply = "the timezone in IANA format, such as Europe/Stockholm or America/California" if name else "the current time, in a format such as 05:23PM or 17:23"
    await CLIENT.edit_message(sent, embed = discord.Embed(
        title = "TimeyWimey Setup 2/2",
        description = f"Great! Now send {reply}."
    ))

    entered = await CLIENT.wait_for_message(
        author = msg.author,
        channel = msg.channel,
        timeout = TIMEOUT
    )
    if entered is None:
        await CLIENT.edit_message(sent, embed = discord.Embed(
            title = "Timeout",
            description = f"More than {TIMEOUT_STR} of inactivity"
        ))
        return

    reply = None
    zone_str = None

    if name:
        zone_str = entered.clean_content
        reply = "Location mode automatically handles daylight saving time"
    else:
        utc = entered.timestamp
        date = None
        try:
            date = dateutil.parser.parse(entered.clean_content, default = utc, ignoretz = True)
        except (ValueError, OverflowError):
            await CLIENT.edit_message(sent, embed = discord.Embed(
                title = "Invalid time",
                description = "Failed to parse time and date"
            ))
            return

        exact = date - utc
        offset = round(exact.total_seconds() / OFFSET_ROUND) * OFFSET_ROUND
        offset = datetime.timedelta(seconds = offset)
        exact = utils.time(exact)
        rounded = utils.time(offset)
        if exact == rounded:
            reply = f"UTC offset was {exact.hours:02}:{exact.minutes:02}"
        else:
            reply = f"Exact UTC offset was {exact.hours:02}:{exact.minutes:02}, but using {rounded.hours:02}:{rounded.minutes:02}"

        if abs(offset) >= datetime.timedelta(hours = 24):
            await CLIENT.send_message(msg.channel, "There was more than one day's difference. This is not supported")
            return
        zone_str = datetime.timezone(offset).tzname(datetime.datetime.now())

    user = User(msg.author.id, hour24, zone_str)

    # Ensure it's valid
    if user.invalid:
        await CLIENT.send_message(msg.channel, f"Invalid timezone")
        return
    date = datetime.datetime.now(user.zone)

    now = user.strftime(date)
    user.insert_or_replace()

    await CLIENT.send_message(msg.channel, f"Timezone set up! {reply}. Current time is {now}")
