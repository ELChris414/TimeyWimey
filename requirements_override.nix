{ pkgs, python }:

self: super: {
  fuzzywuzzy = python.overrideDerivation super.fuzzywuzzy (old: {
    buildInputs = old.buildInputs ++ [ self.setuptools-scm ];
  });
  python-dateutil = python.overrideDerivation super.python-dateutil (old: {
    buildInputs = old.buildInputs ++ [ self.setuptools-scm ];
  });
}
