from globalz import *

from collections import namedtuple
import dateutil.tz

Time = namedtuple("Time", "hours minutes")

def time(delta):
    return Time(
        int(delta.total_seconds() / 60 / 60),
        int(abs(delta.total_seconds()) / 60 % 60)
    )

class User:
    def __init__(self, user_id, hour24 = None, zone_str = None):
        self.exists = False

        if hour24 is None or zone_str is None:
            stmt = DB.execute("SELECT hour24, zone FROM users WHERE user_id = ?", (user_id,))
            row = stmt.fetchone()
            if row is None:
                hour24 = False
                zone_str = "UTC"
            else:
                self.exists = True
                hour24   = row[0] if hour24   is None else hour24
                zone_str = row[1] if zone_str is None else zone_str

        self.user_id = user_id
        self.hour24 = hour24
        self.zone_str = zone_str
        self.zone = dateutil.tz.gettz(zone_str)

    @property
    def invalid(self):
        return self.zone is None

    def insert_or_replace(self):
        DB.execute(
            "REPLACE INTO users (user_id, hour24, zone) VALUES (?, ?, ?)",
            (self.user_id, self.hour24, self.zone_str)
        )
        DB.commit()

    def strftime(self, date):
        hour = "%H:%M" if self.hour24 else "%I:%M %p"
        return date.strftime(f"%B %d %Y {hour}")
